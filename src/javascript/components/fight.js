import { controls } from "../../constants/controls";

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    
    firstFighter.inBlock = false;
    secondFighter.inBlock = false;
    firstFighter.CritAllow = true;
    secondFighter.CritAllow = true;
    firstFighter.MaxHp = firstFighter.health;
    secondFighter.MaxHp = secondFighter.health;
    firstFighter.healthPresentage = 100;
    secondFighter.healthPresentage = 100;
    firstFighter.loser = false;
    secondFighter.loser = false;

    let firstFighterAttack,
      secondFighterAttack,
      firstFighterCritAtk,
      secondFighterCritAtk;

    function healthReducing(victim, volume) {
      if (volume > victim.health) {
        victim.health = 0;
        victim.loser = true;
      } else {
        victim.health = secondFighter.health - volume;
      }

      return victim.health;
    }

    function healthIndicatorsReduce(fighter) {
      let position;
      let volume;
      if (fighter == firstFighter) {
        position = "#left-fighter-indicator";
      } else if (fighter == secondFighter) {
        position = "#right-fighter-indicator";
      }
      volume = fighter.healthPresentage + "%";
      let bar = document.querySelector(position);

      bar.style.width = volume;
    }

    function fighterCritAtk(func, ...codes) {
      let pressed = new Set();

      document.addEventListener("keydown", function (event) {
        pressed.add(event.code);

        for (let code of codes) {
          if (!pressed.has(code)) {
            return;
          }
        }
        pressed.clear();

        func();
      });

      document.addEventListener("keyup", function (event) {
        pressed.delete(event.code);
      });
    }

    document.addEventListener(
      "keydown",
      (firstFighterAttack = (e) => {
        if (e.code == controls.PlayerOneAttack) {
          let dmg = getDamage(firstFighter, secondFighter);
          healthReducing(secondFighter, dmg);
          secondFighter.healthPresentage =
            (secondFighter.health / secondFighter.MaxHp) * 100;
          healthIndicatorsReduce(secondFighter);
          if (secondFighter.healthPresentage == 0) {
            winner = firstFighter;
          }
        }
      })
    );

    document.addEventListener(
      "keydown",
      (secondFighterAttack = (e) => {
        if (e.code == controls.PlayerTwoAttack) {
          let dmg = getDamage(secondFighter, firstFighter);
          healthReducing(firstFighter, dmg);
          firstFighter.healthPresentage =
            (firstFighter.health / firstFighter.MaxHp) * 100;
          healthIndicatorsReduce(firstFighter);
          if (firstFighter.healthPresentage == 0) {
            winner = secondFighter;
          }
        }
      })
    );

    firstFighterCritAtk = () => {
      if (firstFighter.CritAllow) {
        firstFighter.isCritical = true;

        let dmg = getDamage(firstFighter, secondFighter);
        healthReducing(secondFighter, dmg);
        secondFighter.healthPresentage =
          (secondFighter.health / secondFighter.MaxHp) * 100;
        healthIndicatorsReduce(secondFighter);
        if (secondFighter.healthPresentage == 0) {
          winner = firstFighter;
        }

        firstFighter.CritAllow = false;
        firstFighter.isCritical = false;

        setTimeout(function () {
          firstFighter.CritAllow = true;
        }, 10000);
      }
    };

    secondFighterCritAtk = () => {
      if (secondFighter.CritAllow) {
        secondFighter.isCritical = true;

        let dmg = getDamage(secondFighter, firstFighter);
        healthReducing(firstFighter, dmg);
        firstFighter.healthPresentage =
          (firstFighter.health / firstFighter.MaxHp) * 100;
        healthIndicatorsReduce(firstFighter);
        if (firstFighter.healthPresentage == 0) {
          winner = secondFighter;
        }

        secondFighter.CritAllow = false;
        secondFighter.isCritical = false;

        setTimeout(function () {
          secondFighter.CritAllow = true;
        }, 10000);
      }
    };

    fighterCritAtk(
      firstFighterCritAtk,
      ...controls.PlayerOneCriticalHitCombination
    );
    fighterCritAtk(
      secondFighterCritAtk,
      ...controls.PlayerTwoCriticalHitCombination
    );

    document.addEventListener("keydown", (e) => {
      if (e.code == controls.PlayerOneBlock) {
        firstFighter.inBlock = true;
        document.removeEventListener("keydown", firstFighterAttack);
      }
    });

    document.addEventListener("keyup", (e) => {
      if (e.code == controls.PlayerOneBlock) {
        firstFighter.inBlock = false;
        document.addEventListener("keydown", firstFighterAttack);
      }
    });

    document.addEventListener("keydown", (e) => {
      if (e.code == controls.PlayerTwoBlock) {
        secondFighter.inBlock = true;
        document.removeEventListener("keydown", secondFighterAttack);
      }
    });

    document.addEventListener("keyup", (e) => {
      if (e.code == controls.PlayerTwoBlock) {
        firstFighter.inBlock = false;
        document.addEventListener("keydown", firstFighterAttack);
      }
    });
 
    resolve(winner);
  });
}

export function getDamage(attacker, defender) {
  // return damage
  let hitPower = getHitPower(attacker);
  let blockPower = getBlockPower(defender);
  let damage;

  if (attacker.isCritical) {
    return (damage = attacker.attack * 2);
  } else if (defender.inBlock) {
    damage = hitPower - blockPower;
    if (damage > 0) {
      return damage;
    } else return 0;
  } else {
    return (damage = hitPower);
  }
}

export function getHitPower(fighter) {
  // return hit power
  let criticalHitChance = Math.random() + 1;

  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  let dodgeChance = Math.random() + 1;

  return fighter.defense * dodgeChance;
}
