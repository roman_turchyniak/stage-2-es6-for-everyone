import { fight } from "../fight";
import { showModal } from "./modal";

export function showWinnerModal(fighter) {
  // call showModal function 
  showModal({title: `${fighter.name} WIN`})
}
